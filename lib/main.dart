import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:food_order_app/provider/myprovider.dart';
import 'package:food_order_app/screens/checkout.dart';
import 'package:food_order_app/screens/detail_screen.dart';
import 'package:food_order_app/screens/homepage.dart';
import 'package:food_order_app/screens/landing_page.dart';
import 'package:food_order_app/screens/login_screen.dart';
import 'package:food_order_app/screens/profile_screen.dart';
import 'package:food_order_app/screens/registration_screen.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(FoodOrder());
}

class FoodOrder extends StatelessWidget {
  const FoodOrder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<MyProvider>(
      create: (context) => MyProvider(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Fudooda',
        theme: ThemeData(
          primaryColor: Color(0xffff3f6f),
        ),
        home: Scaffold(
          body: HomePage(),
        ),
      ),
    );
  }
}
