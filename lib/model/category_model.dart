import 'package:flutter/material.dart';

class CategoryModel {
  String? image;
  String? name;
  CategoryModel({@required this.image, @required this.name});
}
