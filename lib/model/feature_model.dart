import 'package:flutter/material.dart';

class FeatureModel {
  final String? foodTitle;
  final String? foodSubtitle;
  final String? foodImage;
  final double? rating;
  final double? price;
  FeatureModel(
      {@required this.foodImage,
      @required this.foodSubtitle,
      @required this.foodTitle,
      @required this.price,
      @required this.rating});
}
