import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:food_order_app/model/category_model.dart';
import 'package:food_order_app/model/feature_model.dart';

class MyProvider with ChangeNotifier {
  List<CategoryModel> categoryModelList = [];
  CategoryModel? categoryModel;
  Future<void> getCategoryProduct() async {
    List<CategoryModel> list = [];
    QuerySnapshot querySnapshot =
        await FirebaseFirestore.instance.collection("homecategory").get();
    querySnapshot.docs.forEach((categorydata) {
      categoryModel = CategoryModel(
        image: categorydata["image"],
        name: categorydata["name"],
      );
      list.add(categoryModel!);
    });
    categoryModelList = list;
    notifyListeners();
  }

  List<CategoryModel> get getCategoryModelList {
    return categoryModelList;
  }

  List<FeatureModel> featureModelList = [];
  FeatureModel? featureModel;
  Future<void> getFeatureFood() async {
    List<FeatureModel> list = [];
    QuerySnapshot querySnapshot =
        await FirebaseFirestore.instance.collection("homefeaturefood").get();
    querySnapshot.docs.forEach((homeFeatureFood) {
      featureModel = FeatureModel(
        foodImage: homeFeatureFood["image"],
        foodSubtitle: homeFeatureFood["foodSubtitle"],
        foodTitle: homeFeatureFood["foodTitle"],
        price: homeFeatureFood["price"],
        rating: homeFeatureFood["rating"],
      );
      list.add(featureModel!);
    });
    featureModelList = list;
    notifyListeners();
  }

  List<FeatureModel> get getFeatureModelList {
    return featureModelList;
  }
}
