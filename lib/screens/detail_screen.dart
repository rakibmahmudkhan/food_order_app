import 'package:flutter/material.dart';
import 'package:food_order_app/widget/mybutton.dart';

class DetailScreen extends StatelessWidget {
  Widget _buildSingleDetailText({
    String? title,
    String? subTitle,
    String? trailingTitle,
    String? trailingSubTitle,
  }) {
    return Container(
      height: 100,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title!,
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 22,
                ),
              ),
              Text(
                subTitle!,
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 22,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 7,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                trailingTitle!,
                style: TextStyle(
                    color: Color(0xffff3f6f),
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                trailingSubTitle!,
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 22,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color(0xfffef0f7),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
            size: 30,
          ),
          onPressed: () {},
        ),
      ),
      body: Container(
        child: Stack(
          children: [
            Column(
              children: [
                Expanded(
                  child: Container(
                    color: Color(0xfffef0f7),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    width: double.infinity,
                    color: Color(0xfff8f8f8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 130,
                          alignment: Alignment.bottomLeft,
                          child: Text(
                            "Chicken Broast",
                            style: TextStyle(
                                color: Color(0xffff3f6f),
                                fontSize: 30,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Text(
                          "Radisson Blu Hotel",
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                        Container(
                          height: 100,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            //crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "\$50",
                                    style: TextStyle(
                                        color: Color(0xff04d4ee),
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Container(
                                    width: 110,
                                    height: 35,
                                    decoration: BoxDecoration(
                                      color: Colors.lightBlue[100],
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        GestureDetector(
                                          onTap: () {},
                                          child: Icon(
                                            Icons.add,
                                            size: 25,
                                            color: Color(0xff04d4ee),
                                          ),
                                        ),
                                        Text(
                                          "1",
                                          style: TextStyle(
                                              color: Color(0xff04d4ee),
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        GestureDetector(
                                          onTap: () {},
                                          child: Icon(
                                            Icons.remove,
                                            size: 25,
                                            color: Color(0xff04d4ee),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        _buildSingleDetailText(
                            title: "Weight",
                            subTitle: "120gm",
                            trailingTitle: "Mix Together",
                            trailingSubTitle: "Milk Egg, Dip"),
                        _buildSingleDetailText(
                            title: "Calories",
                            subTitle: "Chicken in this",
                            trailingTitle: "430 cal",
                            trailingSubTitle: "Mixture"),
                        MyButton(name: "Submit", onPressed: () {}),
                      ],
                    ),
                  ),
                )
              ],
            ),
            Positioned(
              left: 100,
              child: Image(
                height: 250,
                width: 230,
                image: AssetImage("assets/images/chickenbrost.png"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
