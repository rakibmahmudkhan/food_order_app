import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:food_order_app/provider/myprovider.dart';

import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  Widget _buildSingleFeature(
      {String? foodTitle,
      String? foodSubTitle,
      String? rating,
      String? price,
      String? image}) {
    return Stack(
      alignment: Alignment.topRight,
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 40.0),
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(20)),
            width: 200,
            height: 210,
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  foodTitle!,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
                Text(
                  foodSubTitle!,
                  style: TextStyle(fontSize: 14, color: Colors.grey),
                ),
                Container(
                  height: 50,
                  width: double.infinity,
                  child: Row(
                    children: [
                      Icon(
                        Icons.star,
                        color: Colors.yellow[700],
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        "$rating Rating",
                        style: TextStyle(fontSize: 14, color: Colors.grey),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Text(
                        "\$$price",
                        style: TextStyle(
                            color: Color(0xff04d4ee),
                            fontWeight: FontWeight.bold,
                            fontSize: 18),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        Positioned(
          child: CircleAvatar(
              maxRadius: 50,
              backgroundColor: Color(0xff04d4ee),
              backgroundImage: NetworkImage(image!)),
        )
      ],
    );
  }

  Widget _buildSingleCategory({String? image, String? name}) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: Container(
        width: 180,
        height: 200,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 120,
              height: 120,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 80,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                      image: NetworkImage(image!),
                    )),
                  ),
                  Text(
                    name!,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTopPart() {
    return Expanded(
      child: Container(
        color: Colors.pinkAccent,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              height: 40,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                      onPressed: () {
                        _scaffoldKey.currentState!.openDrawer();
                      },
                      icon: Icon(
                        Icons.sort,
                        color: Colors.white,
                        size: 35,
                      )),
                  IconButton(
                      onPressed: () {
                        FirebaseAuth.instance.signOut();
                      },
                      icon: Icon(
                        Icons.notifications_none_sharp,
                        color: Colors.white,
                        size: 35,
                      )),
                ],
              ),
            ),
            Expanded(
                child: Container(
              child: Column(
                children: [
                  Container(
                    height: 120,
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      children: [
                        CircleAvatar(
                          maxRadius: 45,
                          backgroundColor: Colors.white,
                          child: CircleAvatar(
                            maxRadius: 40,
                            backgroundImage:
                                AssetImage('assets/images/profile.png'),
                          ),
                        ),
                        Container(
                          height: 80,
                          width: 280,
                          child: ListTile(
                            title: Text(
                              "Are you Hungry ?",
                              style: TextStyle(
                                  fontSize: 23,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }

  MyProvider? myProvider;

  _buildFeatureProduct() {
    return Expanded(
      child: Container(
        child: Column(
          children: [
            Container(
              height: 210,
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: myProvider!.getFeatureModelList.length,
                      itemBuilder: (context, index) => _buildSingleFeature(
                          foodTitle:
                              myProvider!.getFeatureModelList[index].foodTitle,
                          foodSubTitle: myProvider!
                              .getFeatureModelList[index].foodSubtitle,
                          image:
                              myProvider!.getFeatureModelList[index].foodImage,
                          price: myProvider!.getFeatureModelList[index].price
                              .toString(),
                          rating: myProvider!.getFeatureModelList[index].rating
                              .toString())),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBottomPart() {
    return Expanded(
      flex: 2,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        color: Color(0xfff2f3f4),
        child: Column(
          children: [
            Container(
              height: 230,
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    height: 210,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: myProvider!.getCategoryModelList.length,
                        itemBuilder: (context, index) => _buildSingleCategory(
                            name: myProvider!.getCategoryModelList[index].name,
                            image:
                                myProvider!.getCategoryModelList[index].image)),
                  ),
                ],
              ),
            ),
            Container(
              width: double.infinity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Featured",
                    style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff04d4ee)),
                  ),
                  Text(
                    "see all",
                    style: TextStyle(fontSize: 20, color: Color(0xff04d4ee)),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            _buildFeatureProduct(),
          ],
        ),
      ),
    );
  }

  Widget _buildMyDrawer() {
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage('assets/images/profile.png'),
            ),
            accountName: Text("Rakib"),
            accountEmail: Text("rmk@gmail"
                ".com"),
          ),
          ListTile(
            leading: Icon(
              Icons.home,
              color: Colors.pinkAccent,
              size: 30,
            ),
            title: Text(" HomePage"),
          ),
          ListTile(
            leading:
                Icon(Icons.contact_phone, color: Colors.pinkAccent, size: 30),
            title: Text("Contact Us"),
          ),
          ListTile(
            leading: Icon(Icons.info, color: Colors.pinkAccent, size: 30),
            title: Text("About Page"),
          ),
          ListTile(
            leading:
                Icon(Icons.shopping_cart, color: Colors.pinkAccent, size: 30),
            title: Text("Order"),
          ),
          ListTile(
            leading:
                Icon(Icons.exit_to_app, color: Colors.pinkAccent, size: 30),
            title: Text("Logout"),
          ),
        ],
      ),
    );
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    myProvider = Provider.of<MyProvider>(context);
    myProvider!.getCategoryProduct();
    myProvider!.getFeatureFood();
    return Scaffold(
      key: _scaffoldKey,
      drawer: _buildMyDrawer(),
      body: SafeArea(
        child: Container(
          child: Column(
            children: [_buildTopPart(), _buildBottomPart()],
          ),
        ),
      ),
    );
  }
}

//Color(0xff04d4ee),
