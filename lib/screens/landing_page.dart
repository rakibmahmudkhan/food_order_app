import 'package:flutter/material.dart';
import 'package:food_order_app/screens/welcome_screen.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xffff8a78),
              Color(0xffff7275),
              Color(0xffff3f6f),
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Hero(
                  tag: 'appLogTag',
                  child: SizedBox(
                    child: Image.asset('assets/images/logo.png'),
                    height: 55.0,
                  ),
                ),
                SizedBox(
                  width: 10.0,
                ),
                Text(
                  'Fudooda',
                  style: TextStyle(
                    fontSize: 50,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontFamily: 'MuseoModerno',
                  ),
                ),
              ],
            ),
            Text(
              'The food of your choice',
              style: TextStyle(
                  fontStyle: FontStyle.italic,
                  fontSize: 17,
                  color: Colors.white70),
            ),
            SizedBox(height: 120),
            GestureDetector(
              onTap: () {
                // goto   welcome screen  ,if registration is done,goto
                // food menu page
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return WelcomeScreen();
                }));
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 100, vertical: 15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(32),
                ),
                child: Text(
                  'Explore',
                  style: TextStyle(fontSize: 20, color: Color(0xffff3f6f)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
