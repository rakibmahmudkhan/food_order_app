import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:food_order_app/screens/homepage.dart';
import 'package:food_order_app/screens/registration_screen.dart';
import 'package:food_order_app/widget/haveaccountornot.dart';
import 'package:food_order_app/widget/mybutton.dart';
import 'package:food_order_app/widget/mypasswordtextfield.dart';
import 'package:food_order_app/widget/mytextfield.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController? emailControler;
  TextEditingController? passwordControler;

  bool isLoading = false;
  UserCredential? authResult;

  void submit() async {
    setState(() {
      isLoading = true;
    });
    try {
      authResult = await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: emailControler!.text, password: passwordControler!.text);
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return HomePage();
      }));
    } catch (error) {
      Alert(
        context: context,
        title: "Signup Failed",
        desc: error.toString(),
        type: AlertType.error,
      ).show();

      setState(() {
        isLoading = false;
      });
    }

    setState(() {
      isLoading = false;
    });
  }

  void validation() {
    if (emailControler!.text.length > 7 && passwordControler!.text.length > 3) {
      try {
        if (authResult!.user!.email == emailControler!.text) {
          setState(() {
            isLoading = false;
          });
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return HomePage();
          }));
        }
      } catch (error) {
        setState(() {
          isLoading = false;
        });
        Alert(
          context: context,
          title: "LogIn Failed",
          desc: error.toString(),
          type: AlertType.error,
        ).show();
      }
    } else {
      setState(() {
        isLoading = false;
      });
      Alert(
        context: context,
        title: "Login Failed",
        desc: "All fields are required",
        type: AlertType.error,
      ).show();
    }
  }

  Widget _buildAllTextFiledPlace() {
    return Center(
      child: Container(
        height: 300,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MyTextField(
              title: "Email",
              controller: emailControler,
            ),
            SizedBox(
              height: 10,
            ),
            MyPasswordTextField(
              title: "Password",
              controller: passwordControler,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildButtonPart() {
    return isLoading == false
        ? MyButton(
            name: "Login",
            onPressed: () {
              validation();
            },
          )
        : Center(
            child: CircularProgressIndicator(),
          );
  }

  Widget _buildHaveAccountNotPart() {
    return HaveAccountOrNot(
      subTitle: "I Don't Have An Account? ",
      title: "Sign Up",
      onPressed: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return RegistrationScreen();
        }));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xffff8a78),
              Color(0xffff7275),
              Color(0xffff3f6f),
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Hero(
              tag: 'appLogTag',
              child: SizedBox(
                child: Image.asset('assets/images/logo.png'),
                height: 125.0,
              ),
            ),
            Text(
              'Fudooda',
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold,
                color: Colors.white,
                fontFamily: 'MuseoModerno',
              ),
            ),
            SizedBox(height: 40),
            _buildAllTextFiledPlace(),
            _buildButtonPart(),
            SizedBox(
              height: 10,
            ),
            _buildHaveAccountNotPart()
          ],
        ),
      ),
    );
  }
}
