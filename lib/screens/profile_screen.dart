import 'dart:io';

import 'package:flutter/material.dart';
import 'package:food_order_app/screens/homepage.dart';
import 'package:food_order_app/widget/mybutton.dart';
import 'package:food_order_app/widget/mypasswordtextfield.dart';
import 'package:food_order_app/widget/mytextfield.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  bool isEdit = false;
  TextEditingController? nameControler;
  TextEditingController? emailControler;
  TextEditingController? phoneNumberControler;
  TextEditingController? addressControler;
  TextEditingController? passwordControler;
  bool isLoading = false;

  File? image;
  Future<void> getImage({ImageSource? imageSource}) async {
    XFile? pickedFile = await ImagePicker().pickImage(source: imageSource!);
    if (pickedFile != null) {
      image = File(pickedFile.path);
    }
  }

  void validation() {
    if (emailControler!.text.length > 7 &&
        passwordControler!.text.length > 3 &&
        nameControler!.text.length > 2 &&
        phoneNumberControler!.text.length > 10 &&
        addressControler!.text.length > 2) {
      Alert(
        context: context,
        title: "Signup Failed",
        desc: "All fields are required",
        type: AlertType.error,
      ).show();
    } else {
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<void> myDialogueBox() {
    return showDialog<void>(
        builder: (context) => Builder(builder: (BuildContext context) {
              return AlertDialog(
                content: SingleChildScrollView(
                  child: ListBody(
                    children: [
                      ListTile(
                        leading: Icon(Icons.camera),
                        title: Text("Camera"),
                        onTap: () {
                          getImage(imageSource: ImageSource.camera);
                          Navigator.of(context).pop();
                        },
                      ),
                      ListTile(
                        leading: Icon(Icons.photo_album),
                        title: Text("Gallery"),
                        onTap: () {
                          getImage(imageSource: ImageSource.gallery);
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                ),
              );
            }),
        context: context,
        barrierDismissible: false);
  }

  final GlobalKey<ScaffoldState> scaffold = GlobalKey<ScaffoldState>();

  Widget _buildAllTextField() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          MyTextField(
            controller: nameControler,
            title: "Enter your name",
          ),
          SizedBox(
            height: 15.0,
          ),
          MyTextField(
            controller: emailControler,
            title: "Enter your mail",
          ),
          SizedBox(
            height: 15.0,
          ),
          MyTextField(
            controller: phoneNumberControler,
            title: "Phone Number",
          ),
          SizedBox(
            height: 15.0,
          ),
          MyTextField(
            controller: addressControler,
            title: "Address",
          ),
          SizedBox(
            height: 15.0,
          ),
          MyPasswordTextField(
            controller: passwordControler,
            title: "Password",
          ),
          MyButton(
              name: "Update",
              onPressed: () {
                validation();
              }),
        ],
      ),
    );
  }

  _buildSingleContainer({String? name}) {
    return Container(
      height: 60,
      alignment: Alignment.centerLeft,
      decoration: BoxDecoration(
          color: Color(0xfffeeaf4), borderRadius: BorderRadius.circular(8)),
      padding: EdgeInsets.only(left: 10),
      child: Text(
        name!,
        style: TextStyle(fontSize: 15, color: Colors.grey[700]),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffold,
      appBar: AppBar(
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
            size: 30,
          ),
          onPressed: () {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => HomePage(),
              ),
            );
          },
        ),
        centerTitle: true,
        backgroundColor: Color(0xffff3f6f),
        title: Text(
          "Profile",
          style: TextStyle(
              fontWeight: FontWeight.bold, color: Colors.white, fontSize: 20),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10, top: 15.0),
            child: GestureDetector(
              onTap: () {
                setState(() {
                  isEdit = !isEdit;
                });
              },
              child: Text(
                isEdit == false ? "Edit" : "Close",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 20),
              ),
            ),
          ),
        ],
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: Stack(
          children: [
            Column(
              children: [
                Container(
                  width: double.infinity,
                  height: 230,
                  color: Color(0xffff3f6f),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      CircleAvatar(
                        maxRadius: 75,
                        backgroundColor: Colors.white,
                        child: CircleAvatar(
                          maxRadius: 70,
                          backgroundImage:
                              AssetImage("assets/images/profile.png"),
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(
                    flex: 3,
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                      color: Colors.white,
                      child: isEdit == false
                          ? Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                _buildSingleContainer(
                                    name: "Rakib Mahmud Khan"),
                                _buildSingleContainer(name: "rmk@gmail.com"),
                                _buildSingleContainer(name: "01644341067"),
                                _buildSingleContainer(name: "Nikunjo, Dhaka"),
                              ],
                            )
                          : _buildAllTextField(),
                    )),
              ],
            ),
            isEdit != false
                ? Positioned(
                    top: 183,
                    left: 240,
                    child: CircleAvatar(
                      backgroundColor: Colors.white,
                      maxRadius: 20,
                      child: IconButton(
                        onPressed: () {
                          myDialogueBox();
                        },
                        icon: Icon(
                          Icons.edit,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
