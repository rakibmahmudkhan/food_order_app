import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:food_order_app/screens/homepage.dart';
import 'package:food_order_app/screens/login_screen.dart';
import 'package:food_order_app/widget/haveaccountornot.dart';
import 'package:food_order_app/widget/mybutton.dart';
import 'package:food_order_app/widget/mypasswordtextfield.dart';
import 'package:food_order_app/widget/mytextfield.dart';

import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class RegistrationScreen extends StatefulWidget {
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  TextEditingController? nameControler;
  TextEditingController? emailControler;
  TextEditingController? phoneNumberControler;
  TextEditingController? addressControler;
  TextEditingController? passwordControler;

  bool isLoading = false;
  UserCredential? authResult;

  void submit() async {
    setState(() {
      isLoading = true;
    });
    try {
      final authResult = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(
              email: emailControler!.text, password: passwordControler!.text);

      if (authResult.user!.email == emailControler!.text) {
        setState(() {
          isLoading = false;
        });
        await FirebaseFirestore.instance
            .collection("UserData")
            .doc(authResult.user!.uid)
            .set({
          "UserName": nameControler!.text,
          "UserEmail": emailControler!.text,
          "UserId": authResult.user!.uid,
          "UserNumber": phoneNumberControler!.text,
          "UserAddress": addressControler!.text,
        });
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return HomePage();
        }));
        setState(() {
          isLoading = false;
        });
      }
    } catch (error) {
      setState(() {
        isLoading = false;
      });
    }
  }

  void validation() {
    if (emailControler!.text.length > 7 &&
        passwordControler!.text.length > 3 &&
        nameControler!.text.length > 2 &&
        phoneNumberControler!.text.length > 10 &&
        addressControler!.text.length > 2) {
      Alert(
        context: context,
        title: "Signup Failed",
        desc: "All fields are required",
        type: AlertType.error,
      ).show();
    } else {
      setState(() {
        isLoading = false;
        submit();
      });
    }
  }

  Widget _buildAllTextField() {
    return Container(
      height: 400,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          MyTextField(
            controller: nameControler,
            title: "Enter your name",
          ),
          SizedBox(
            height: 15.0,
          ),
          MyTextField(
            controller: emailControler,
            title: "Enter your mail",
          ),
          SizedBox(
            height: 15.0,
          ),
          MyTextField(
            controller: phoneNumberControler,
            title: "Phone Number",
          ),
          SizedBox(
            height: 15.0,
          ),
          MyTextField(
            controller: addressControler,
            title: "Address",
          ),
          SizedBox(
            height: 15.0,
          ),
          MyPasswordTextField(
            controller: passwordControler,
            title: "Password",
          ),
        ],
      ),
    );
  }

  Widget _buildButton() {
    return isLoading == false
        ? MyButton(
            name: "Sign Up",
            onPressed: () {
              validation();
            },
          )
        : Center(
            child: CircularProgressIndicator(),
          );
  }

  Widget _buildHaveAccountorNotPart() {
    return HaveAccountOrNot(
      onPressed: () {
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) {
            return LoginScreen();
          }),
        );
      },
    );
  }

  final GlobalKey<ScaffoldState> scaffold = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffold,
      body: ModalProgressHUD(
        inAsyncCall: isLoading!,
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Color(0xffff8a78),
                  Color(0xffff7275),
                  Color(0xffff3f6f),
                ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Hero(
                  tag: 'appLogTag',
                  child: SizedBox(
                    child: Image.asset('assets/images/logo.png'),
                    height: 125.0,
                  ),
                ),
                Text(
                  'Fudooda',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontFamily: 'MuseoModerno',
                  ),
                ),
                SizedBox(height: 40),
                _buildAllTextField(),
                _buildButton(),
                _buildHaveAccountorNotPart(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
