import 'package:flutter/material.dart';
import 'package:food_order_app/screens/login_screen.dart';
import 'package:food_order_app/screens/registration_screen.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xffff8a78),
              Color(0xffff7275),
              Color(0xffff3f6f),
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Hero(
                  tag: 'appLogTag',
                  child: SizedBox(
                    child: Image.asset('assets/images/logo.png'),
                    height: 55.0,
                  ),
                ),
                SizedBox(
                  width: 10.0,
                ),
                Text(
                  'Fudooda',
                  style: TextStyle(
                    fontSize: 50,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontFamily: 'MuseoModerno',
                  ),
                ),
              ],
            ),
            Text(
              'The food of your choice',
              style: TextStyle(
                  fontStyle: FontStyle.italic,
                  fontSize: 17,
                  color: Colors.white70),
            ),
            SizedBox(height: 120),
            GestureDetector(
              onTap: () {
                // goto login   screen
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return LoginScreen();
                }));
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 110, vertical: 15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(32),
                ),
                child: Text(
                  'Log In',
                  style: TextStyle(fontSize: 20, color: Color(0xffff3f6f)),
                ),
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            GestureDetector(
              onTap: () {
                // goto registration
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return RegistrationScreen();
                }));
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 100, vertical: 15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(32),
                ),
                child: Text(
                  'Sign Up',
                  style: TextStyle(fontSize: 20, color: Color(0xffff3f6f)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
