import 'package:flutter/material.dart';

class HaveAccountOrNot extends StatelessWidget {
  final String? title;
  final String? subTitle;
  final VoidCallback? onPressed;
  HaveAccountOrNot({this.title, this.subTitle, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          subTitle!,
          style: TextStyle(
            color: Colors.white,
            fontSize: 14,
          ),
        ),
        InkWell(
          onTap: onPressed!,
          child: Text(
            title!,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w600,
              fontSize: 15,
            ),
          ),
        ),
      ],
    );
  }
}
