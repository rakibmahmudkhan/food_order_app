import 'package:flutter/material.dart';

class MyPasswordTextField extends StatefulWidget {
  final TextEditingController? controller;
  final String? title;

  MyPasswordTextField({this.controller, this.title});

  @override
  State<MyPasswordTextField> createState() => _MyPasswordTextFieldState();
}

class _MyPasswordTextFieldState extends State<MyPasswordTextField> {
  bool obscureText = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 40),
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(32.0),
        color: Colors.white,
      ),
      child: TextField(
        controller: widget.controller,
        obscureText: obscureText,
        cursorColor: Color(0xffff3f6f),
        decoration: InputDecoration(
          suffixIcon: GestureDetector(
            onTap: () {
              setState(() {
                obscureText = !obscureText;
              });
            },
            child: Icon(
              obscureText == true ? Icons.visibility : Icons.visibility_off,
            ),
          ),
          border: InputBorder.none,
          hintText: widget.title,
          hintStyle: TextStyle(
            fontWeight: FontWeight.normal,
            color: Color(0xffff7275),
          ),
        ),
      ),
    );
  }
}
